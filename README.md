## About
This script will install **PHP without apache** and **latest composer** in your system for laravel

tested on Ubuntu 16.04 and 18.04.

## Usage
1. Clone this repo or [Download](https://github.com/zxp86021/php-setup/archive/master.zip)
2. a. for **NOT** Ubuntu 18.04 Users, Please run `install.sh`  
    b. for Ubuntu 18.04 Users, Please run `install-1804.sh`
  
and it will do everything you need.
